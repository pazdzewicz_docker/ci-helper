#!/usr/bin/env bash
HELM="$(command -v helm)"

if [ -z "${HELM}" ]
then
  echo "HELM is missing"
  exit 1
fi

if [ -z "${GITLAB_HELM_USER}" ]
then
  echo "GITLAB_HELM_USER environment variable is missing"
  exit 1
fi

if [ -z "${GITLAB_HELM_TOKEN}" ]
then
  echo "GITLAB_HELM_TOKEN environment variable is missing"
  exit 1
fi

if [ -z "${RELEASE_NAME}" ]
then
  echo "RELEASE_NAME environment variable is missing"
  exit 1
fi

if [ -z "${RELEASE_PROJECT}" ]
then
  echo "RELEASE_PROJECT environment variable is missing"
  exit 1
fi

if [ -z "${K8S_NAMESPACE}" ]
then
  echo "K8S_NAMESPACE environment variable is missing"
  exit 1
fi

if [ -z "${K8S_ENV}" ]
then
  echo "K8S_ENV environment variable is missing"
  exit 1
fi

echo "Deploy the Project \"${CI_PROJECT_NAME}\", Release \"${RELEASE_NAME}:${RELEASE_VERSION}\" (\"${RELEASE_PROJECT}\") with Image \"${CONTAINER_IMAGE}\" to \"${K8S_ENV}\" with Namespace \"${K8S_NAMESPACE}\""

CUSTOM_VALUES=""

if [ -d "${CI_PROJECT_DIR}/.helm" ]
then
  if [ -f "${CI_PROJECT_DIR}/.helm/${K8S_ENV}.yaml" ]
  then
    CUSTOM_VALUES_PATH="${CI_PROJECT_DIR}/.helm/${K8S_ENV}.yaml"
    echo "Use Custom Values from ${CUSTOM_VALUES_PATH}"
    CUSTOM_VALUES="--values ${CUSTOM_VALUES_PATH}"
  fi
fi

REPO_CMD="${HELM} repo add --username ${GITLAB_HELM_USER} --password ${GITLAB_HELM_TOKEN} ${RELEASE_NAME} https://gitlab.com/api/v4/projects/${RELEASE_PROJECT}/packages/helm/stable"

echo "Execute repo add command"
echo "${REPO_CMD}"

${REPO_CMD}

UPGRADE_CMD="${HELM} upgrade ${RELEASE_NAME} ${RELEASE_NAME}/${RELEASE_NAME} --version ${RELEASE_VERSION} --create-namespace --namespace=${K8S_NAMESPACE} --install ${CUSTOM_VALUES} --set env="${K8S_ENV}" ${@}"


echo "Execute upgrade command"
echo "${UPGRADE_CMD}"

${UPGRADE_CMD}

echo "Helm Deployment Completed Successfully"

if [ ! -z "${WEB_HOST}" ]
then
  echo "ENVIRONMENT URL: ${WEB_HOST}"
fi

echo "Set DYNAMIC_ENVIRONMENT_URL to deploy.env"
echo "DYNAMIC_ENVIRONMENT_URL=https://${WEB_HOST}" >> deploy.env

if [ ! -z "${CONTAINER_IMAGE}" ]
then
  echo "IMAGE: ${CONTAINER_IMAGE}"
fi