#!/bin/bash
set -e
set -o pipefail

if [ -z "${CI_JOB_TOKEN}" ]
then
  echo "CI_JOB_TOKEN environment variable is missing"
  exit 1
fi

if [ -z "${CI_API_V4_URL}" ]
then
  echo "CI_API_V4_URL environment variable is missing"
  exit 1
fi

if [ -z "${CI_PROJECT_ID}" ]
then
  echo "CI_PROJECT_ID environment variable is missing"
  exit 1
fi

if [ -z "${CI_PROJECT_NAME}" ]
then
  echo "CI_PROJECT_NAME environment variable is missing"
  exit 1
fi

if [ -z "${BUILD_ARCHIVE}" ]
then
  echo "BUILD_ARCHIVE environment variable is missing"
  exit 1
fi

if [ -z "${CI_COMMIT_SHORT_SHA}" ]
then
  echo "CI_COMMIT_SHORT_SHA environment variable is missing"
  exit 1
fi

if [ -n "${CI_COMMIT_TAG}" ]
then
  TAG="${CI_COMMIT_TAG}"
else
  TAG="${CI_COMMIT_SHORT_SHA}";
fi


curl --silent --fail --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "${BUILD_ARCHIVE}" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${TAG}/${BUILD_ARCHIVE}"