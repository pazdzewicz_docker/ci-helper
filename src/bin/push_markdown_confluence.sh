#!/bin/bash
set -e
set -o pipefail

# Get needed tools
CURL="$(command -v curl)"
JQ="$(command -v jq)"
SED="$(command -v sed)"
CAT="$(command -v cat)"
EVAL="$(command -v eval)"
PANDOC="$(command -v pandoc)"

# Check if Vars are there
if [ -z "${CONFLUENCE_API_URL}" ]
then
  echo "CONFLUENCE_API_URL environment variable is missing"
  exit 1
fi

if [ -z "${CONFLUENCE_USER}" ]
then
  echo "CONFLUENCE_USER environment variable is missing"
  exit 1
fi

if [ -z "${CONFLUENCE_SPACE}" ]
then
  echo "CONFLUENCE_SPACE environment variable is missing"
  exit 1
fi

if [ -z "${CONFLUENCE_ACCESS_TOKEN}" ]
then
  echo "CONFLUENCE_ACCESS_TOKEN environment variable is missing"
  exit 1
fi

if [ -z "${CONFLUENCE_PAGE_TITLE}" ]
then
  echo "CONFLUENCE_PAGE_TITLE environment variable is missing"
  exit 1
fi

if [ -z "${CONFLUENCE_PAGE_ID}" ]
then
  echo "CONFLUENCE_PAGE_ID environment variable is missing"
  exit 1
fi

README_FILE="${1}"
if [ -z "${README_FILE}" ]
then
  echo "README_FILE argument is missing (first script arg)"
  exit 1
fi

# Build Auth string
echo "# Build Auth string"
AUTH="${CONFLUENCE_USER}:${CONFLUENCE_ACCESS_TOKEN}"
# Debug only
#echo ${AUTH}

# Build request
echo "# Build request"
REQUEST="${CONFLUENCE_API_URL}/rest/api/content/${CONFLUENCE_PAGE_ID}"
# Debug only
#echo ${REQUEST}

# Send Request and get current version
echo "# Send Request and get current version"
VERSION=$(${CURL} --fail --silent --request GET --url "${REQUEST}" --user "${AUTH}" --header "Accept: application/json" | ${JQ} -r '.version.number')
echo "${VERSION}"

# Increment the Version
echo "# Increment the Version"
VERSION=$((VERSION + 1))

# Modify bot-info.html
echo "# Modify bot-info.html"
BOT_FILE="/ci_tools/bot-info.html"
${SED} -i -e "s|repo_url|${CI_PROJECT_URL}|g" "${BOT_FILE}"

# Parsing Anachore Reports
echo "# Parsing Anachore Reports"
PACKAGES_FILE="/ci_tools/packages.md"
PACKAGES_REPORT_FILE="anchore-reports/image-content-os-report.json"
if [ -f "${PACKAGES_REPORT_FILE}" ]
then
  # shellcheck disable=SC2129 # Consider using { cmd1; cmd2; } >> file instead of individual redirects.
  echo "\`\`\`" >> "${PACKAGES_FILE}"
  ${JQ} '[.content | sort_by(.package) | .[] | {package: .package, version: .version}]' "${PACKAGES_REPORT_FILE}" >> "${PACKAGES_FILE}"
  # shellcheck disable=SC2129 # Consider using { cmd1; cmd2; } >> file instead of individual redirects.
  echo "\`\`\`" >> "${PACKAGES_FILE}"
fi


VULNERABILITIES_FILE="/ci_tools/vulnerabilities.md"
VULNERABILITIES_REPORT_FILE="anchore-reports/image-vuln-report.json"
if [ -f "${VULNERABILITIES_REPORT_FILE}" ]
then
  # shellcheck disable=SC2129 # Consider using { cmd1; cmd2; } >> file instead of individual redirects.
  echo "\`\`\`" >> "${VULNERABILITIES_FILE}"
  ${JQ} '[.vulnerabilities | group_by(.package) | .[] | {package: .[0].package, vuln: [.[].vuln]}]' "${VULNERABILITIES_REPORT_FILE}" >> "${VULNERABILITIES_FILE}"
  # shellcheck disable=SC2129 # Consider using { cmd1; cmd2; } >> file instead of individual redirects.
  echo "\`\`\`" >> "${VULNERABILITIES_FILE}"
fi

# Build Site
echo "# Build Site"
INFO=$(${CAT} "${BOT_FILE}")
README=$(${EVAL} "${PANDOC}" "${README_FILE}" --from=gfm --to=html5)

if [ -f "${PACKAGES_REPORT_FILE}" ]
then
  PACKAGES=$(${EVAL} "${PANDOC}" "${PACKAGES_FILE}" --from=gfm --to=html5)
fi

if [ -f "${VULNERABILITIES_REPORT_FILE}" ]
then
  VULNERABILITIES=$(${EVAL} "${PANDOC}" "${VULNERABILITIES_FILE}" --from=gfm --to=html5)
fi

# Build Output
echo "# Build Output"
OUTPUT=$(${JQ} -c --slurp -aR . <<< "${INFO} ${README} ${VULNERABILITIES} ${PACKAGES}")

# Create the payload
echo "# Create the payload"
PAYLOAD="payload.json"
echo '{"id":"'"${CONFLUENCE_PAGE_ID}"'","type":"page","title":"'"${CONFLUENCE_PAGE_TITLE}"'","space":{"key":"'"${CONFLUENCE_SPACE}"'"},"body":{"storage":{"value":'"${OUTPUT}"',"representation":"storage"}},"version":{"number":'"${VERSION}"'}}' > "${PAYLOAD}"
# Debug only
#echo ${PAYLOAD}

# Send a PUT request to Confluence API to update the page using our payload
echo "# Send a PUT request to Confluence API to update the page using our payload"
curl --fail -u "${AUTH}" -X PUT -H 'Content-Type: application/json' -d @"${PAYLOAD}"  "${REQUEST}"
