#!/usr/bin/env bash
HELM="$(command -v helm)"

if [ -z "${HELM}" ]
then
  echo "HELM is missing"
  exit 1
fi

if [ -z "${RELEASE_NAME}" ]
then
  echo "RELEASE_NAME environment variable is missing"
  exit 1
fi

if [ -z "${K8S_NAMESPACE}" ]
then
  echo "K8S_NAMESPACE environment variable is missing"
  exit 1
fi

UNINSTALL_CMD="${HELM} uninstall ${RELEASE_NAME} --namespace=${K8S_NAMESPACE} --wait"
${UNINSTALL_CMD}