FROM debian:bookworm

RUN mkdir /ci_tools/ && \
    apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y --no-install-recommends \
                       ca-certificates \
                       curl \
                       jq \
                       git \
                       asciidoctor \
                       python3 \
                       zip \
                       pandoc && \
    apt-get clean && \
    rm -r /var/lib/apt/lists/*

COPY src/bot-info.html /ci_tools/bot-info.html
COPY src/packages.md /ci_tools/packages.md
COPY src/vulnerabilities.md /ci_tools/vulnerabilities.md

COPY src/bin/push_markdown_confluence.sh /ci_tools/push_markdown_confluence.sh
COPY src/bin/push_adoc_confluence.sh /ci_tools/push_adoc_confluence.sh
COPY src/bin/auto_merge_request.sh /ci_tools/auto_merge_request.sh
COPY src/bin/upload_generic_package.sh /ci_tools/upload_generic_package.sh
COPY src/bin/kubernetes_deploy.sh /ci_tools/kubernetes_deploy.sh
COPY src/bin/kubernetes_stop_deploy.sh /ci_tools/kubernetes_stop_deploy.sh
COPY src/bin/wait_for_it.sh /ci_tools/wait_for_it.sh

RUN chmod +x /ci_tools/push_markdown_confluence.sh \
             /ci_tools/push_adoc_confluence.sh  \
             /ci_tools/auto_merge_request.sh \
             /ci_tools/kubernetes_deploy.sh \
             /ci_tools/kubernetes_stop_deploy.sh \
             /ci_tools/wait_for_it.sh \
             /ci_tools/upload_generic_package.sh && \
    curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash && \
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
    chmod +x kubectl && \
    mv kubectl /usr/local/bin/kubectl