# ci-helper

This Image contains a few helper tools

## Parent image

- `debian:bookworm`

## Dependencies

None

## Entrypoint & CMD

Since this Image is only used during CI we don't need an entrypoint.

## Functions

- `/ci_tools/push_markdown_confluence.sh` - Updates the Confluence Documentation (more in Usage).
- `/ci_tools/push_adoc_confluence.sh` - Updates the Confluence Documentation (more in Usage).
- `/ci_tools/auto_merge_request.sh` - Creates a merge request via CI/CD (more in Usage).
- `/ci_tools/upload_generic_package.sh` - Uploads a Generic Package via CI/CD (more in Usage).
- `/ci_tools/kubernetes_deploy.sh` - Deploys a Helm Package via CI/CD (more in Usage).
- `/ci_tools/kubernetes_stop_deploy.sh` - Stops a previous Helm Package Deployment via CI/CD (more in Usage).

## Build Image

### Automated Build

Through the new build pipeline all of our Docker images are built via Gitlab CI/CD and pushed to the Gitlab Container Registry. You usually don't need to build the image on your own, just push your commits to the git. The Image will be tagged after your branch name.

After the automated build is finished you can pull the image:

```
docker pull registry.gitlab.com/pazdzewicz_docker/ci-helper:BRANCH
```

All images also have a master tag, just replace the BRANCH with "master". The "latest" Tag is only an alias for the "master" tag.

### Manual Build

You can also build the Docker image on your local pc:

```
docker build -t "myci-helper:latest" .
```

### Build Options

Build Options are Arguments in the Dockerfile (`--build-arg`):

- None

### Security Check during Build

Before we release Docker Containers into the wild it is required to run the Anchore security checks over the Pipeline (this doesn't apply to manual built images). You can download the current artifacts on the Pipelines page (the download drop-down).

## Environment Variables:

For `/ci_tools/auto_merge_request.sh`:
- `PRIVATE_TOKEN` - Gitlab Private Token for `auto_merge_request.sh` (Im pazdzewicz Gitlab wird diese Variable den Runnern unter `GITLAB_PRIVATE_TOKEN` bereitgestellt)

For `/ci_tools/push_adoc_confluence.sh`:
- `CONFLUENCE_API_URL` -  The Confluence API URL
- `CONFLUENCE_SPACE` -  The Confluence Space
- `CONFLUENCE_USER` - The Confluence API User
- `CONFLUENCE_ACCESS_TOKEN` - The Confluence API Token [MANAGE](https://id.atlassian.com/manage/api-tokens)
- `CONFLUENCE_PAGE_ID` - The ID of the Confluence Page
- `CONFLUENCE_PAGE_TITLE` - The Title of the Confluence Page

For `/ci_tools/push_markdown_confluence.sh`:
- `CONFLUENCE_API_URL` -  The Confluence API URL
- `CONFLUENCE_SPACE` -  The Confluence Space
- `CONFLUENCE_USER` - The Confluence API User
- `CONFLUENCE_ACCESS_TOKEN` - The Confluence API Token [MANAGE](https://id.atlassian.com/manage/api-tokens)
- `CONFLUENCE_PAGE_ID` - The ID of the Confluence Page
- `CONFLUENCE_PAGE_TITLE` - The Title of the Confluence Page

For `/ci_tools/upload_generic_package.sh`:
- `BUILD_ARCHIVE` - Path to ZIP Archive to Upload
- `TAG` - Custom Package Tag (optional)


## Ports

- None

## Usage

This image is not very simple, because it is used during CI.

### Generic Usage

```
#####
# Push Readme to Confluence (for better overview)
#####

push_readme:
  stage: doc
  image: registry.gitlab.com/pazdzewicz_docker/ci-helper:latest
  script:
    # Markdown README
    - /ci_tools/push_markdown_confluence.sh README.md
    # ADOC README
    - /ci_tools/push_adoc_confluence.sh README.adoc
    # Create new Merge Request
    - HOST=${CI_PROJECT_URL} PRIVATE_TOKEN=${GITLAB_PRIVATE_TOKEN} /ci_tools/auto_merge_request.sh
    # Uploads a new generic Package to Gitlab
    - /ci_tools/upload_generic_package.sh
  only:
    - master
```

### Sync Remote with local Gitlab

To Sync a remote Repository you can use this snippet and modify it.
It will create a new merge request to the default branch in the current repository if there are any changes from the remote master branch.

```
github_updates:
  stage: updates
  image:
    name: registry.gitlab.com/pazdzewicz_docker/ci-helper:latest
  variables:
    # public github remote repository
    GITHUB_URL: "https://github.com/rseichter/automx2.git"
    # private gitlab remote repository
    GITHUB_URL: "https://${GITLAB_CLOUD_USER}:${GITLAB_CLOUD_TOKEN}@gitlab.com/pazdzewicz/teamspeak.git"
    # New Branch Name in local repository
    NEW_BRANCH_NAME: "master-github"
  script:
    - if [ -n "$(git remote | grep gitremote)" ]; then git remote remove gitremote; fi
    - git remote add gitremote ${GITHUB_URL}
    - git fetch gitremote
    - git checkout master
    - git pull gitremote master
    - if [ -n "$(git branch | grep ${NEW_BRANCH_NAME})" ]; then git branch -d ${NEW_BRANCH_NAME}; fi
    - git checkout -b ${NEW_BRANCH_NAME}
    - if [ -n "$(git remote | grep origin)" ]; then git remote remove origin; fi
    - git remote add origin https://${GITLAB_PRIVATE_USER}:${GITLAB_PRIVATE_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git
    - | 
      if [ -n "$(git diff-index HEAD)" ]
      then
        git push origin ${NEW_BRANCH_NAME}
        HOST=${CI_PROJECT_URL} CI_COMMIT_REF_NAME="${NEW_BRANCH_NAME}" PRIVATE_TOKEN=${GITLAB_PRIVATE_TOKEN} /ci_tools/auto_merge_request.sh
      fi
  allow_failure: true
  only:
    - schedules
```